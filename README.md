# pymud

A python mud server

Installation should be simple:

`git clone https://gitlab.com/bubthegreat/pymud.git`
`cd pymud`
`pip install .`

Alternatively:

`pip install https://gitlab.com/bubthegreat/pymud.git`

After installation, you should have a command: `pymud`.

```
(venv) bub@m18-bub:~/Development/pymud$ pymud
04/22/2024 10:09:03 PM INFO     [pymud] Loaded pymud library.
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command look
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command dig
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command goto
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command help
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command commands
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command say
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command up
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command down
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command north
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command south
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command east
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command west
04/22/2024 10:09:03 PM INFO     [pymud.commands] Initialized command quit
04/22/2024 10:09:03 PM INFO     [pymud.room] Room created: 1 with exits: {}
04/22/2024 10:09:03 PM INFO     [pymud.mud] Serving on ('0.0.0.0', 8888)
```

Connect with your telnet client of choice.  I use telnet when I'm feeling lazy with `telnet localhost 8888`

You should see a welcome screen:

```
Enter your name: 
bub
Welcome, bub!
l
A Stark White Room: 1

This is the most beautiful room you've ever laid eyes on.  It's completely bare and stark white
painted cinderblocks are incredibly welcoming.  In the middle of the room you see a sign with the 
number 1 painted in what appears to be an unpleasant amount of blood.

[Exits: None]


say Huh
You say 'Huh'
bob arrives in a puff of pink smoke.
bob says 'Huh.'
bob disappears in an explosion of pink mist.

commands
Available Commands:

commands
dig
down
east
goto
help
look
north
quit
say
south
up
west
```

Type `commands` for a list of commands available in the mud, and for the love of all that's holy, contribute if you want to!
