from pymud.commands.say import SayCommand

import pytest
from unittest.mock import AsyncMock

class MockPlayer:
    def __init__(self, name):
        self.name = name
        self.send_to_player = AsyncMock()

    def __str__(self):
        return self.name

@pytest.mark.asyncio
async def test_say_command_others_in_room():
    # Create mock player objects
    player_mock = MockPlayer("Player1")
    other_player_mock = MockPlayer("Player2")

    # Set up others_in_room
    player_mock.others_in_room = [other_player_mock]

    # Create a mock world
    world_mock = AsyncMock()

    # Call the function
    await SayCommand.run(world_mock, player_mock, "Hello!")

    # Check if player.send_to_player was called with the correct message
    player_mock.send_to_player.assert_called_once_with("You say 'Hello!'")
    # Check if other_player.send_to_player was called with the correct message
    expected_message = "\u001b[33mPlayer1 says 'Hello!'\u001b[0m"
    other_player_mock.send_to_player.assert_called_once_with(expected_message)
