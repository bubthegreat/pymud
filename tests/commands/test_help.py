from pymud.commands.help import HelpCommand
from pymud.commands import BaseCommand

import pytest
from unittest.mock import AsyncMock

@pytest.mark.asyncio
async def test_help_initial_and_final_message():
    # Create a mock player object
    player_mock = AsyncMock()
    player_mock.send_to_player = AsyncMock()

    # Create a mock world
    world_mock = AsyncMock()

    # Call the function
    await HelpCommand.run(world_mock, player_mock, None)

    # Check if player.send_to_player was called with the correct initial message
    player_mock.send_to_player.assert_any_call("Available Help Topics:\n")
    # Check if player.send_to_player was called with the final newline
    player_mock.send_to_player.assert_any_call("\n")


@pytest.mark.asyncio
async def test_help_call_count_equals_commands():
    # Create a mock player object
    player_mock = AsyncMock()
    player_mock.send_to_player = AsyncMock()

    # Create a mock world
    world_mock = AsyncMock()

    # Call the function
    await HelpCommand.run(world_mock, player_mock, None)

    # Check if player.send_to_player was called the same number of commands that exist in the keys
    player_mock.send_to_player.call_count = len(BaseCommand.available_helpfiles) - 2


@pytest.mark.asyncio
async def test_do_commands_individual_commands():
    # Create a mock player object
    player_mock = AsyncMock()
    player_mock.send_to_player = AsyncMock()

    # Create a mock world
    world_mock = AsyncMock()

    # Call the function
    await HelpCommand.run(world_mock, player_mock, None)

    # Check if player.send_to_player was called with each command in COMMANDS
    for command in BaseCommand.available_helpfiles.keys():
        expected_call = f"{command}"
        player_mock.send_to_player.assert_any_call(expected_call)