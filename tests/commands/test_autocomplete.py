from pymud.commands import BaseCommand


def test_no_duplicate_autocomplete_options():
    subclasses = BaseCommand.__subclasses__()
    all_autocomplete_options = {}
    for subclass in subclasses:
        for aco in subclass.autocomplete_options:
            if aco in all_autocomplete_options:
                raise ValueError(f"Duplicate autocomplete options found for value `{aco}` in command `{subclass.command_key}`.  Already registered with `{all_autocomplete_options[aco].command_key}`")
            else:
                all_autocomplete_options[aco] = subclass
