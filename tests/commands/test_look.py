import pytest
from unittest.mock import AsyncMock, MagicMock
from pymud.commands.look import LookCommand
from pymud.room import Room


@pytest.mark.asyncio
@pytest.mark.parametrize("room_exits, others_in_room, expected_message", [
    ({"north": None, "south": None}, ["Player1", "Player2"], "Room Name\n\nRoom Description\n\n[Exits: north south]\n\nPlayer1 is here.\nPlayer2 is here.\n"),
    ({}, [], "Room Name\n\nRoom Description\n\n[Exits: None]\n\n"),
    ({"north": None, "south": None}, [], "Room Name\n\nRoom Description\n\n[Exits: north south]\n\n"),
    ({"up": None, "down": None, "east": None, "west": None}, [], "Room Name\n\nRoom Description\n\n[Exits: east west up down]\n\n"),
    ({"north": None, "south": None}, ["Player1", "Player2", "Player3"], "Room Name\n\nRoom Description\n\n[Exits: north south]\n\nPlayer1 is here.\nPlayer2 is here.\nPlayer3 is here.\n")
])
async def test_look_command(room_exits, others_in_room, expected_message):
    room_mock = MagicMock(spec=Room)
    room_mock.name = "Room Name"
    room_mock.description = "Room Description"
    room_mock.exits = room_exits

    player_mock = AsyncMock()
    player_mock.room = room_mock
    player_mock.others_in_room = others_in_room

    await LookCommand.run(None, player_mock, None)

    player_mock.send_to_player.assert_called_once_with(expected_message)
