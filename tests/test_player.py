import pytest
from unittest.mock import AsyncMock, Mock

from pymud.player import Player



@pytest.mark.asyncio
async def test_player_send_to_player(writer_mock):
    reader = AsyncMock()
    writer = writer_mock

    player = Player("TestPlayer", reader, writer)
    message = "Test message"
    await player.send_to_player(message)
    
    writer.write.assert_called_once_with(f"{message}\n\r".encode())
    assert writer.drain.awaited

@pytest.mark.asyncio
async def test_player_others_in_room(player_mock, player_mock1, player_mock2):    
    player_mock.room = Mock()
    player_mock.room.players = [player_mock, player_mock1, player_mock2]

    assert len(player_mock.others_in_room) == 2
    assert player_mock1 in player_mock.others_in_room
    assert player_mock2 in player_mock.others_in_room


def test_default_attributes(player_mock):
    """
    Test to verify that default attributes (hitpoints, mana, moves) are set when no values are passed in.
    """
    assert player_mock.hitpoints == 100
    assert player_mock.mana == 100
    assert player_mock.moves == 100


def test_nondefault_attributes(nondefault_player_mock):
    """
    Test to verify that default attributes (hitpoints, mana, moves) are set when no values are passed in.
    """
    assert nondefault_player_mock.hitpoints == 1
    assert nondefault_player_mock.mana == 2
    assert nondefault_player_mock.moves == 3
