import pytest

@pytest.mark.asyncio
async def test_default_room_name(default_room):
    """Verify the default name of a room."""
    assert default_room.name == f"A Stark White Room: {default_room.id}"

@pytest.mark.asyncio
async def test_default_room_description(default_room):
    """Verify the default description of a room."""
    assert default_room.description == f"This is the most beautiful room you've ever laid eyes on.  It's completely bare and stark white painted cinderblocks are incredibly welcoming.  In the middle of the room you see a sign with the number {default_room.id} painted in what appears to be an unpleasant amount of blood."

@pytest.mark.asyncio
async def test_room_count(full_room):
    """Verify the count of players in a full room."""
    assert len(full_room.players) == 3

@pytest.mark.asyncio
async def test_room_name(full_room):
    """Verify the name of a room."""
    assert full_room.name == "Fake Room Name"
@pytest.mark.asyncio
async def test_room_description(full_room):
    """Verify the description of a room."""
    assert full_room.description == "Fake Room Description"

@pytest.mark.asyncio
async def test_player_counts_in_full_room(full_room):
    """Verify the count of other players in a room from a player's perspective."""
    assert len(full_room.players[0].others_in_room) == 2
    assert len(full_room.players[1].others_in_room) == 2
    assert len(full_room.players[2].others_in_room) == 2
