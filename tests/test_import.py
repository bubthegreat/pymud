import importlib
import os
import pytest

def get_modules_and_submodules():
    root_dir = os.path.abspath(os.path.join(os.path.dirname(__file__), "..", "src", "pymud"))
    modules = []
        
    for dirpath, dirnames, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename.endswith(".py") and filename != "__init__.py":
                module_path = os.path.relpath(os.path.join(dirpath, filename), root_dir)
                module_name = module_path.replace(os.sep, ".")[:-3]  # Remove '.py'
                modules.append(module_name)
    
    return modules

# This verifies that we can actually import all our individual modules and stops us from
# having code that never is reached by the main function that might have missing imports
# or bad folder definitions.  Example - this caught that we had bad requirements because
# one of our imports that we aren't hitting requires sqlalchemy and that isn't installed
# in the existing requirements.
@pytest.mark.parametrize("module_name", get_modules_and_submodules())
def test_imports(module_name):
    try:
        importlib.import_module(f"pymud.{module_name}")
    except ImportError as e:
        assert False, f"Failed to import pymud.{module_name}: {e}"
