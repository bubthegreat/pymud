import pytest
from unittest.mock import AsyncMock, Mock

from pymud.player import Player
from pymud.room import Room



class WriterMock(Mock):
    """Generic class to mock a writer properly."""
    write = Mock()  # The write function does not return a coroutine
    drain = AsyncMock() # The drain object DOES return a coroutine.

@pytest.fixture(scope='function')
def writer_mock():
    return WriterMock()

@pytest.fixture(scope='function')
def nondefault_player_mock():
    nondefault_player = Player(
        'Player',
        AsyncMock(),
        writer_mock,
        hitpoints=1,
        mana=2,
        moves=3
    )
    return nondefault_player

@pytest.fixture(scope='function')
def player_mock():
    return Player('Player', AsyncMock(), writer_mock)

@pytest.fixture(scope='function')
def player_mock1():
    return Player('Player1', AsyncMock(), writer_mock)

@pytest.fixture(scope='function')
def player_mock2():
    return Player('Player2', AsyncMock(), writer_mock)

@pytest.fixture
def full_room(player_mock, player_mock1, player_mock2):
    fixture_room = Room(
        room_id=1,
        name="Fake Room Name",
        description="Fake Room Description",
        players = [player_mock, player_mock1, player_mock2],
    )
    player_mock.room = fixture_room
    player_mock1.room = fixture_room
    player_mock2.room = fixture_room
    return fixture_room

@pytest.fixture
def default_room():
    fixture_room = Room(
        room_id=1,
    )
    return fixture_room
