"""Various exception classes for pymud."""


class DuplicateRoomsException(Exception):
    """Raise when duplicate rooms detected."""
    

class NoRoomsException(Exception):
    """Raise when no rooms detected."""


class DuplicateAreasException(Exception):
    """Raise when duplicate areas detected."""


class KillServer(Exception):
    """Raise when server is killed."""