SCORE_BASE = """
\u001b[37;1m{name} the {title}.\u001b[0m
--------------------------------
hitpoints: {current_hp}/{max_hp}
mana:      {current_mp}/{max_mp}
moves:     {current_mv}/{max_mv}
--------------------------------
"""

def format_score(player):
    formatted = SCORE_BASE.format(
        name=player.name,
        title=player.title,
        current_hp=player.current_hp,
        current_mp=player.current_mp,
        current_mv=player.current_mv,
        max_hp=player.base_hp,
        max_mp=player.base_mp,
        max_mv=player.base_mv,
    )
    return formatted
