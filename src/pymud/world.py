import logging
from pymud.room import Room
from pymud.commands import command_handler
from pymud.commands.look import LookCommand

LOGGER = logging.getLogger(__name__)

class World:
    def __init__(self):
        self.players = []
        self.rooms = {1: Room(1)}

    async def add_player(self, player):
        LOGGER.info("Added player %s", player)
        self.players.append(player)
        await self.move_player_to_room(player, 1)

    async def move_player_to_room(self, player, room_id):
        if player.room:
            player.room.players.remove(player)
            LOGGER.info("Player %s removed from room %s.", player, room_id)
            # Now that we've removed them from the room we can tell others they left.
            # TODO: Move this logic into the do_move logic.
            for remaining_player in player.room.players:
                remaining_player.writer.write(f"\u001b[35m{player.name} disappears in a puff of pink smoke.\u001b[0m\n\r".encode())
                await remaining_player.writer.drain()
        if room_id in self.rooms:
            room = self.rooms[room_id]
        else:
            LOGGER.info("No existing room found for room %s - creating.")
            room = Room(room_id)
            self.rooms[room_id] = room
        for existing_player in room.players:
            existing_player.writer.write(f"\u001b[35m{player.name} arrives in a puff of pink smoke.\u001b[0m\n\r".encode())
            await existing_player.writer.drain()
            self.rooms[room_id] = room
        room.players.add(player)
        LOGGER.info("Player %s added to room %s", player, room)
        player.room = room
        # Make sure they look at the room they get dropped into.
        await LookCommand.run(self, player, None)


    async def handle_player(self, player):
        while True:
            data = await player.reader.readline()
            message = data.decode().strip()
            await command_handler(self, player, message)
