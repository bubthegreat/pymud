import logging

LOGGER = logging.getLogger(__name__)

class PlayerBase:
    def __init__(self, name, reader, writer):
        self.name = name
        self.reader = reader
        self.writer = writer
        self.room = None

        LOGGER.info(f"Made player {name} in room {self.room}")

    def __str__(self):
        return self.name

    async def send_to_player(self, message):
        encoded_message = f"{message}\n\r".encode()
        self.writer.write(encoded_message)
        await self.writer.drain()

    @property
    def others_in_room(self):
        return {player for player in self.room.players if player is not self}


class Player(PlayerBase):
    def __init__(self, name, reader, writer, hitpoints=100, mana=100, moves=100):
        super().__init__(name, reader, writer)
        self.hitpoints = hitpoints
        self.mana = mana
        self.moves = moves

        LOGGER.info(f"Initialized {name} with HP: {self.hitpoints}, Mana: {self.mana}, Moves: {self.moves}")
