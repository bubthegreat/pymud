import logging
import asyncio

from pymud.commands.look import LookCommand
from . import BaseCommand

LOGGER = logging.getLogger(__name__)

OPPOSITE_DIRECTIONS = {
    'up': 'down',
    'down': 'up',
    'east': 'west',
    'west': 'east',
    'north': 'south',
    'south': 'north',
}

async def _do_move(world, player, direction):
    old_room = player.room
    LOGGER.debug("Attempting to move player %s %s from %s.", player, direction, old_room.id)
    next_room = old_room.exits.get(direction)
    if next_room:
        await world.move_player_to_room(player, next_room.id)
        LOGGER.debug("Moved player %s %s from room %s to room %s", player, direction, old_room, next_room)
        await LookCommand.run(world, player, None)
        #TODO: Make this happen as part of the base command handling
        await asyncio.sleep(0.5)
    else:
        await player.send_to_player("You cannot go that way.")


class UpCommand(BaseCommand):

    command_key = 'up'
    helpfile = """
    Here is a helpfile for the up command.
    """
    autocomplete_options = ['u']
    async def run(world, player, args):
        await _do_move(world, player, 'up')

class DownCommand(BaseCommand):

    command_key = 'down'
    helpfile = """
    Here is a helpfile for the down command.
    """
    autocomplete_options = ['d']
    async def run(world, player, args):
        await _do_move(world, player, 'down')

class NorthCommand(BaseCommand):

    command_key = 'north'
    helpfile = """
    Here is a helpfile for the north command.
    """
    autocomplete_options = ['n']
    async def run(world, player, args):
        await _do_move(world, player, 'north')

class SouthCommand(BaseCommand):

    command_key = 'south'
    helpfile = """
    Here is a helpfile for the south command.
    """
    autocomplete_options = ['s']
    async def run(world, player, args):
        await _do_move(world, player, 'south')

class EastCommand(BaseCommand):

    command_key = 'east'
    helpfile = """
    Here is a helpfile for the east command.
    """
    autocomplete_options = ['e']
    async def run(world, player, args):
        await _do_move(world, player, 'east')

class WestCommand(BaseCommand):

    command_key = 'west'
    helpfile = """
    Here is a helpfile for the west command.
    """
    autocomplete_options = ['w']
    async def run(world, player, args):
        await _do_move(world, player, 'west')
