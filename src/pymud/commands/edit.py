import logging


from . import BaseCommand

LOGGER = logging.getLogger(__name__)

def _edit_room(world, room_id, attribute, value):
    try:
        room_id = int(room_id)
    except:
        raise ValueError(f"Unable to cast room_id {room_id} to an int - check your syntax and try again!")

    room = world.rooms.get(room_id)

    if not room:
        raise ValueError(f"No room for id {room_id} found - check your syntax and try again!")

    if not hasattr(room, attribute):
        raise ValueError(f"No attribute {attribute} found on room {room_id} - check your syntax and try again!")
    
    setattr(room, attribute, value)

    return room


EDITORS = {
    'room': _edit_room,
}

class CommandsCommand(BaseCommand):

    command_key = 'edit'
    helpfile = """
    Edit an object (Currently only room attributes, but we expect to
    be able to edit more things later, so we're going to set this
    up to have the format for it later.

    Syntax

        edit <object_type> <object_id> <object attribute> <object value>

    Example

        edit room description This is a new description for your room.
    """

    async def run(world, player, args):

        argsplit = args.split()
        try: 
            object_type = argsplit[0]
            object_id = argsplit[1]
            object_attribute = argsplit[2]
            object_attribute_value = ' '.join(argsplit[3:])
        except IndexError:
            await player.send_to_player(f"Syntax error.  Please see helpfile for edit command.")
            return

        edit_func = EDITORS.get(object_type)
        if not edit_func:
            player.send_to_player(f"Not a valid object type to edit.  Available object types are: {' '.join(EDITORS.keys())}")
            return
        try: 
            _ = edit_func(world, object_id, object_attribute, object_attribute_value)
            LOGGER.info("Player %s updated %s %s attribute %s.", player, object_type, object_id, object_attribute)

            await player.send_to_player(f"Updated {object_type} {object_id} {object_attribute}")
        except ValueError as err:
            await player.send_to_player(f"Error updating object: {err}")
