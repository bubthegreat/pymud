import abc
import logging
import os
import importlib

LOGGER = logging.getLogger(__name__)


# # Trying to change this to a base command that we subclass so that when
# # it's subclassed it has all the stuff it needs for a command to be
# # registered and have it work with the expected aspects like world, room, and args
class BaseCommand(abc.ABC):

    available_commands = {}
    available_helpfiles = {}
    _default_autocomplete_options = []


    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)
        LOGGER.debug("Initialized command %s", cls.command_key)
        cls.available_commands[cls.command_key] = cls
        cls.available_helpfiles[cls.command_key] = cls.helpfile

        # Set a default autocomplete of an empty list for consistency
        # if the command has no autocomplete options.
        if not hasattr(cls, 'autocomplete_options'):
            cls.autocomplete_options = cls._default_autocomplete_options


    @property
    @abc.abstractmethod
    def command_key(cls):
        pass


    @property
    @abc.abstractmethod
    def helpfile(cls):
        pass

    @classmethod
    @abc.abstractmethod
    async def run(cls, world, player, args):
        pass


def _autocomplete_command(command):
    command_class = None

    all_commands = sorted(BaseCommand.available_commands.keys())
    available_commands = {}

    for valid_command in all_commands:
        if valid_command.startswith(command):
            LOGGER.debug("Command %s starts with %s", valid_command, command)
            command_class = BaseCommand.available_commands[valid_command]
            if command_class.autocomplete_options and command in command_class.autocomplete_options:
                available_commands[valid_command] = command_class

    LOGGER.debug("available_commands: %s", available_commands)


    count_commands = 0
    command_results = None

    if available_commands:
        LOGGER.debug("Found %s autocomplete options for command %s", len(available_commands), command)
        count_commands = len(available_commands)
        command_results = list(available_commands.values())

    return count_commands, command_results



async def command_handler(world, player, player_input):
    LOGGER.debug("Started handling player %s command %s", player, player_input)
    splitinput = player_input.split()

    # Return with a helpful message if there's no input.
    if not splitinput:
        await player.send_to_player("Huh?")
        return

    # Split out the command and args, and look for a command function that matches.
    command = splitinput[0]
    args = ' '.join(player_input.split()[1:])
    command_class = BaseCommand.available_commands.get(command)


    if command_class:
        LOGGER.info("Player %s ran command %s", player, command_class.command_key)
        await command_class.run(world, player, args)
    else:
        # If we don't find a command class, try the autocomplete and run that.

        command_count, command_classes = _autocomplete_command(command)
        LOGGER.debug("Autocomplete results: %s result.", command_count)

        if command_count == 1:
            command_class = command_classes[0]
            await command_class.run(world, player, args)
            LOGGER.info("Player %s ran command %s", player, command_class.command_key)
        elif command_count > 1:
            await player.send_to_player(f"Did you mean one of these? {' '.join([c.command_key for c in command_classes])}")
        else:
            await player.send_to_player("Huh?")
    LOGGER.debug("Finished handling player %s command %s", player, command)





def _initialize_commands():
    """We're being clever here to import all the commands in this base directory
    and we shouldn't be clever, but I'm going to have to revisit this conce it's 
    more simplified.  For now clever is better than inconsistent.
    """
    # Get the directory of the current module and all the module names under it.
    current_dir = os.path.dirname(__file__)
    module_names = [file[:-3] for file in os.listdir(current_dir) if file.endswith(".py") and file != "__init__.py"]

    # Dynamically import each module
    # TODO: Find a better way to make sure the subclasses get imported without importing all of it.
    for module_name in module_names:
        _ = importlib.import_module(f"{__package__}.{module_name}")


# Call initialize_commands to recursively initialize all subclasses
_initialize_commands()
