from pymud.constants import EXITS_ORDER
from . import BaseCommand

def sort_key(direction):
    return EXITS_ORDER.index(direction)

class LookCommand(BaseCommand):

    command_key = 'look'
    helpfile = """
    This is the helpfile for look.
    """
    autocomplete_options = ['l']

    async def run(world, player, args):
        sorted_directions = sorted(player.room.exits.keys(), key=sort_key)
        message = ''
        message += f"{player.room.name}\n\n{player.room.description}\n\n[Exits: {' '.join(sorted_directions) or 'None'}]\n\n"
        for p in player.others_in_room:
            message += f'{p} is here.\n'
        await player.send_to_player(message)
