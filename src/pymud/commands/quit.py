import logging

from . import BaseCommand

LOGGER = logging.getLogger(__name__)


class DigCommand(BaseCommand):

    command_key = 'quit'
    helpfile = """
    Here is a helpfile for the quit command.
    """
    async def run(world, player, args):
        LOGGER.info("Player %s is quitting.", player)
        world.players.remove(player)
        room = player.room
        room.players.remove(player)
        for p in room.players:
            await p.send_to_player(f"\u001b[35m{player.name} disappears in an explosion of pink mist.\u001b[0m")
        await player.send_to_player("Goodbye!")
        player.writer.close()
        await player.writer.wait_closed()


