import logging
import asyncio

from pymud.room import Room
from pymud.commands.look import LookCommand
from . import BaseCommand

LOGGER = logging.getLogger(__name__)

OPPOSITE_DIRECTIONS = {
    'up': 'down',
    'down': 'up',
    'east': 'west',
    'west': 'east',
    'north': 'south',
    'south': 'north',
}

class DigCommand(BaseCommand):

    command_key = 'dig'
    helpfile = """
    Creates a room in the direction specified and connects the new room to
    the room you call the command from from the opposite direction.  Used
    to create new rooms and link them.

    Syntax:

        dig <direction> <Optional[room_id]>

    Examples:
        
    dig east
        
        Digs a room east and creates a link back to the previous room
    
    
    dig east 1
    
        digs a room east and connects this east exit to room number one
        and a west exit from room number one to the current room.

    """

    async def run(world, player, args):
        args = args.split()
        target_room_id = None
        old_room = player.room

        # Set our direction/target_room_id from args
        if len(args) == 2: 
            direction, target_room_id = args[0], int(args[1])
        else:
            direction = args[0]

        # Get full direction name from partial input: 
        for full_direction in OPPOSITE_DIRECTIONS.keys():
            if full_direction.startswith(direction):
                direction = full_direction

        # Check if there's a room already connected that direction, we don't want
        # to accidentally edit an exit.  Will make other utils to do this with
        # intentional syntax so you can't blow your shit up by being casual.
        if old_room.exits.get(direction) is not None:
            await player.send_to_player("There's already a room that direction!")
            return


        next_room = world.rooms.get(target_room_id)
        if target_room_id and not next_room:
            await player.send_to_player("There is no room with that id.")
            return

        # If we don't find the room from this exit and there's no target room
        # specified, we create a new room before we try to move folks to it.
        if not next_room:
            next_room = Room(
                room_id=max(world.rooms.keys()) + 1,
            )
            world.rooms[next_room.id] = next_room
        next_room.add_exit(OPPOSITE_DIRECTIONS[direction], old_room)
        old_room.add_exit(direction, next_room)
        # We want to set the next_room var consistently so we can use it
        # wether or not it was created just now.

        await world.move_player_to_room(player, next_room.id)
        LOGGER.debug("Moved player %s %s from room %s to new room %s", player, direction, old_room, next_room)
        await LookCommand.run(world, player, None)
        #TODO: Make this happen as part of the base command handling
        await asyncio.sleep(0.5)
