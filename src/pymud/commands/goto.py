import logging
import asyncio

from . import BaseCommand
from pymud.commands.look import LookCommand

LOGGER = logging.getLogger(__name__)

class GotoCommand(BaseCommand):

    command_key = 'goto'
    helpfile = """
    This is the helpfile for goto.
    """

    async def run(world, player, target_room_id):
        target_room_id = int(target_room_id)
        LOGGER.debug("Attempting to poof player %s to %s from %s.", player, target_room_id, player.room.id)
        next_room = world.rooms.get(target_room_id)
        if next_room:
            await world.move_player_to_room(player, target_room_id)
            await LookCommand.run(world, player, None)
            #TODO: Make this happen as part of the base command handling
            await asyncio.sleep(0.5)
        else:
            await player.send_to_player("There is no room like that.")
