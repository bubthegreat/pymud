from . import BaseCommand

class SayCommand(BaseCommand):

    command_key = 'say'
    helpfile = """
    This is the helpfile for the say command.
    """

    async def run(world, player, args):
        await player.send_to_player(f"You say '{args}'")
        for p in player.others_in_room:
            await p.send_to_player(f"\u001b[33m{player} says '{args}'\u001b[0m")

