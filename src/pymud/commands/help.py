
from . import BaseCommand


class HelpCommand(BaseCommand):

    command_key = 'help'
    helpfile = """
    This is the helpfile for the help command.  If you're helping the
    help command maybe...maybe you should type the quit command.
    """

    async def run(world, player, args):
        if not args:
            await player.send_to_player("Available Help Topics:\n")
            for help_key in sorted(BaseCommand.available_helpfiles.keys()):
                await player.send_to_player(help_key)
            await player.send_to_player("\n")
            return
        helpfile = BaseCommand.available_helpfiles.get(args)
        if not helpfile:
            await player.send_to_player("No helpfile available for that.")
        await player.send_to_player(helpfile)
