
from . import BaseCommand


class CommandsCommand(BaseCommand):

    command_key = 'commands'
    helpfile = """
    This is the helpfile for commands command. It tells you the commands
    that you have at your command.
    """

    async def run(world, player, args):
        await player.send_to_player("Available Commands:\n")
        for command in sorted(BaseCommand.available_commands.keys()):
            await player.send_to_player(command)
        await player.send_to_player("\n")
