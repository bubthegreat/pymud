"""This is the main package."""

import asyncio
import logging

from pymud.exceptions import KillServer
from pymud.player import Player
from pymud.world import World
from pymud.commands import command_handler

LOGGER = logging.getLogger(__name__)

async def run():
    world = World()

    async def handle_client(reader, writer):
        name = None
        while not name:
            writer.write("Enter your name: ".encode())
            await writer.drain()
            data = await reader.readline()
            name = data.decode().strip()
            if not name:
                writer.write("You gotta have a name dude.")
                await writer.drain()
        player = Player(name, reader, writer)
        await player.send_to_player(f"\u001b[35mWelcome, {name}!\u001b[0m\n\r")
        await world.add_player(player)

        while True:
            data = await player.reader.readline()
            message = data.decode().strip()
            try:
                await command_handler(world, player, message)
            except ConnectionResetError:
                LOGGER.info("Player %s disconnected.", player)
                return None

    server = await asyncio.start_server(handle_client, '0.0.0.0', 8888)

    try:
        addr = server.sockets[0].getsockname()
        LOGGER.info('Serving on %s', addr)
        await server.serve_forever()
    except (KeyboardInterrupt, KillServer) as exc:
        server.close()
        LOGGER.info(f'Shutting down server: {exc}')

def main():
    """Run main functions."""
    asyncio.run(run(), debug=True)

if __name__ == "__main__":
    main()



