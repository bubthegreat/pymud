import logging

LOGGER = logging.getLogger(__name__)


class Room:
    def __init__(self, room_id, name=None, description=None, players=None):
        self.id = room_id
        self.name = name or f"A Stark White Room: {self.id}"
        self.description = description or f"This is the most beautiful room you've ever laid eyes on.  It's completely bare and stark white painted cinderblocks are incredibly welcoming.  In the middle of the room you see a sign with the number {self.id} painted in what appears to be an unpleasant amount of blood."
        self.players = players or set()
        self._exits = {}
        LOGGER.info("Room created: %s with exits: %s", self.id, self._exits)

    @property
    def exits(self):
        return self._exits

    def add_exit(self, direction, room):
        LOGGER.debug("Added exit to %s: %s - %s", self, direction, room)
        self._exits[direction] = room

    def __repr__(self): 
        return str(self.id)


