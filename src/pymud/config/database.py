# TODO: Decide if we want to bother with a DB implementation.  Both have challenges with complexity in different
#       spots - file based is in the deployment making sure volume persistence happens appropriately and db based
#       simplifies the deployment for volume persistence but means that you have to deal with remote services and
#       secrets, etc. to be able to handle it all.  The more you carry in the DB, the more load and risk you have
#       of it failing by blasting the database, and (for example) implementations like KBK where someone might not
#       have the logic behind the database implementation well defined, or even defined at all, you might have a
#       lot of breakable stuff.

# import os

# from sqlalchemy import create_engine
# from sqlalchemy.orm import sessionmaker


# class DBConfig:
#     """Class instance to make sure that we're giving back the same config objects."""

#     # URL that we'll use to connect to the database.
#     PYMUD_DB_URL = os.environ.get('PYMUD_DB_URL') or 'sqlite:///tutorial.db'

#     def __init__(self):
#         """Empty initialization for config."""
#         self._engine = None
#         self._session = None

#     @property
#     def ENGINE(self):
#         if not self._engine:
#             self._engine = create_engine(self.PYMUD_DB_URL, echo=False)
#         return self._engine

#     @property
#     def SESSION(self):
#         if not self._session:
#             Session = sessionmaker(bind=self.ENGINE)
#             self._session = Session()
#         return self._session

# DB_CONFIG = DBConfig()
