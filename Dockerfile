FROM python:3.7-alpine

COPY --from=ghcr.io/astral-sh/uv:latest /uv /bin/uv

WORKDIR /pymud
COPY . .

RUN uv pip install --system .

EXPOSE 8888

CMD pymud